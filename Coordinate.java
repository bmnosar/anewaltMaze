 /**
  * A class that stores X and Y values to be coordinates of a Location
  * 
  * @author Bryan Nosar
  * @version 1.0 September 27, 2015
  */
class Coordinate{
    
    /**
     * Default Constructor 
     */
    public Coordinate(){
        this(0,0);
    }
    
    /**
     * Constructor
     * 
     * @param xVal The X Value for X coordinate
     * @param yVal The Y Value for Y coordinate
     */
    public Coordinate(int xVal, int yVal){
        this.xVal = xVal;
        this.yVal = yVal;
    }
    
    /**
     * Get the X coordinate
     * 
     * @return xVal The X coordinate
     */
    public int getXVal(){
        return xVal;
    }
    
    /**
     * Get the Y coordinate
     * 
     * @return yVal The Y coordinate
     */
    public int getYVal(){
        return yVal;
    }
    
    /**
     * Set the X coordinate
     * 
     * @param xVal The X coordinate
     */
    public void setXVal(int xVal){
        this.xVal = xVal;
    }
    
    /**
     * Set the Y coordinate
     * 
     * @param yVal The Y coordinate
     */
    public void setYVal(int yVal){
        this.yVal = yVal;
    }

    private int xVal;
    private int yVal;
}