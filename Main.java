import java.io.*;
import java.util.*;


/**
  * This is the main class that controls the main flow of the Maze game. 
  * It can read in the file in order to start the game.
  * 
  * @author Bryan Nosar
  * @version 1.0 September 27, 2015
  */
class Main{
    public static void main(String[] args) {
        
        if (args.length != 1) {
            System.out.println("Invalid use. Must provide filename.");
            System.exit(0);
        }

        System.out.println("Enter your name.");
        Scanner in = new Scanner(System.in);
        String name = in.nextLine();

        String fileName = args[0];
        Maze maze = readFile(fileName);        
        GameState gameState = new GameState(maze, name);
        while (true) {
            System.out.println(maze.print());
            System.out.print("How would you like to move (U,D,L,R,S,G)?");
            String command = in.nextLine();
            String result = gameState.movePlayer(command);
            switch (result) {
                case "win":
                    System.out.println(gameState.printPath());
                    System.out.println("The cake is not a lie!");
                    System.out.println("" +
                        "       _..--'''@   @'''--.._\n" +
                        "     .'   @_/-//-\\/>/>'/ @  '.\n" +
                        "    (  @  /_<//<'/----------^-)\n" +
                        "    |'._  @     //|###########|\n" +
                        "    |~  ''--..@|',|}}}}}}}}}}}|\n" +
                        "    |  ~   ~   |/ |###########|\n" +
                        "    | ~~  ~   ~|./|{{{{{{{{{{{|\n" +
                        "     '._ ~ ~ ~ |,/`````````````\n" +
                        "        ''--.~.|/");
                    System.out.println("Enjoy your Cake! A Winner is you!");
                    System.exit(0);
                    break;
                case "ongoing":
                    break;
                default:
                    System.out.println("Something terrible has happened. :D");
                    break;
            }
        }
    }
    
    
    /**
     * Read in a file to create a Maze object
     * 
     * @param fileName The filename that is passed to the program as an argument
     * @return map The Maze object created from reading in the file
    */
    private static Maze readFile(String fileName) {
    
        FileReader file = null;
        BufferedReader buffer = null;
        Maze map = null;
        try{
            file = new FileReader(fileName);
            buffer = new BufferedReader(file);
            
            int mazeSize = 0;
            String firstLine = "";
            if((firstLine = buffer.readLine()) != null) mazeSize = Integer.parseInt(firstLine);     
        
            map = new Maze(mazeSize);
        
            String line = null;
            for(int y = 0; y < mazeSize; y++){
                for(int x = 0; x < mazeSize; x++){
                    if((line = buffer.readLine()) != null){
                        Location newLocation = new Location(x, y, line, false);
                        newLocation.setInfo(line);
                        map.addLocation(x, y, newLocation);
                    }   
                }
            }
            
        } catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");                
        } catch(IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");                  
        }
        
        return map;
    }
}