/**
  * A class that stores data about a location in a maze. This has coordinates and what the location is.
  * 
  * @author Bryan Nosar
  * @version 1.0 September 27, 2015
  */
class Location{
    
     /**
      * Default Contructor
      */
    public Location(){
        this(0, 0, "?", false);
    }
    
     /**
      * Contructor
      * 
      * @param x The X coordinate of the Location
      * @param y The Y coordinate of the Location
      * @param info The information about the room(Space, wall, exit, player, traveled)
      * @param visited Logic if the player has visited the current Location
      */
    public Location(int x, int y, String info, boolean visited){
        coordinate = new Coordinate(x,y);

        switch(info){
            case "0":
                this.info = " ";
                break;
            case "1":
                this.info = "X";
                break;
            case "2":
                this.info = "E";
                break;
            case "?":
                this.info = "?";
                break;
            case "P":
                this.info = "P";
                break;
            case "T":
                this.info = "T";
                break;
            default:
                System.out.println("error: invalid symbol '" + info + "' read.");
                System.exit(0);
        }

        this.visited = visited;
    }


    private Coordinate coordinate;
    
     /**
      * Set the Coordinate of the Location
      * 
      * @param x The X coordinate of the Location
      * @param y The Y coordinate of the Location
      */
    public void setCoordinate(int x, int y){
        coordinate.setXVal(x);
        coordinate.setYVal(y);
    }
    
    /**
      * Return the X coordinate
      * 
      * @return xVal The X coordinate of the Location
      */
    public int getX(){
        return coordinate.getXVal();
    }

    /**
      * Return the Y coordinate
      * 
      * @return yVal The Y coordinate of the Location
      */
    public int getY(){
        return coordinate.getYVal();
    }
    
    private String info;
    
    /**
      * Return the information about the Location
      * 
      * @return info The information stored in the Location
      */
    public String getInfo(){
        return info;
    }
    
    /**
      *Set the information about the Location
      * 
      * @param info The information to be stored in the Location
      */
    public void setInfo(String info){
        switch(info){
            case "0":
            case " ":
                this.info = " ";
                break;
            case "1":
                this.info = "X";
                break;
            case "2":
                this.info = "E";
                break;
            case "?":
                this.info = "?";
                break;
            case "P":
                this.info = "P";
                break;
            case "T":
                this.info = "T";
                break;
            default:
                System.out.println("error: invalid symbol '" + info + "' read.");
                System.exit(0);
        }
    }


    private boolean visited;
    
    /**
      * Return if the Location was visited
      * 
      * @return visited True if Location was visited, False if Location was not
      */
    public boolean getVisited(){
        return visited;
    }
    
    /**
      * Set the room if it was visited
      * 
      * @param visited True if player visited the Location, False if player has not visited the Location
      */
    public void setVisited(boolean visited){
        this.visited = visited;
    }

}