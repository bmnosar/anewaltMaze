 /**
  * A class that stores a 2D array of Locations to act like a maze.
  * It stores data about the Locations in the maze and the size of the maze
  * 
  * @author Bryan Nosar
  * @version 1.0 September 27, 2015
  */
class Maze{
    
     /**
      * Default Contructor
      */
    public Maze(){
        this(1);
    }
    
    /**
      * Constructor
      * 
      * @param size The size that will be used to make the dimensions of the maze
      */
    public Maze(int size){
        this.size = size;
        maze = new Location[size][size];
    }
    
    private Location[][] maze;
    
    /**
      * Add locations to the maze
      * 
      * @param x The X coordinate
      * @param y The Y coordinate
      * @param location The location to be added into the maze
      */
    public void addLocation(int x, int y, Location location){
        maze[y][x] = location;
    }
    
    /**
      * Get Location at current x,y coordinate
      * 
      * @param x The X coordinate of the Location
      * @param y The Y coordinate of the Location
      * 
      * @return Location Returns a location at the x,y coordinate
      */
    public Location getLocation(int x, int y){
        return maze[y][x];
    }

    private int size;
    
    
    /**
      * Return the size of the Maze
      * 
      * @return size The dimensions of the Maze
      */
    public int getSize() {
        return size;
    }
    
    /**
      * Print the column numbers using a StringBuilder object
      * 
      * @param mazeBuilder A StringBuilder used to print out the first row of the maze, which is the column numbers
      */
    private void appendMazeHeader(StringBuilder mazeBuilder) {
        mazeBuilder.append(" ");    
        mazeBuilder.append(" ");        
        for (int i = 0; i != size; ++i) {
            mazeBuilder.append(i);
            mazeBuilder.append(" ");
        }
        mazeBuilder.append("\n");
    }
    
    /**
      * Print the body of the maze using a StringBuilder object
      * 
      * @param mazeBuilder A StringBuilder used to print out the body of the maze which contains the board and the row numbers
      */
    private void appendMazeBody(StringBuilder mazeBuilder) {
       for (int i = 0; i != size; ++i) {
            mazeBuilder.append(i);
            mazeBuilder.append(" ");
            for ( int j = 0; j != size; ++j) {
            
                if (!maze[i][j].getVisited()) {
                    mazeBuilder.append("?");
                    mazeBuilder.append(" ");
                    continue;
                }
                
                String currentLocation = maze[i][j].getInfo();
                mazeBuilder.append(currentLocation);
                mazeBuilder.append(" ");
            }
            
            mazeBuilder.append("\n");
        }
    }
    
    /**
      * Check if the player can move to the next Location in the maze
      * 
      * @return true If the player can move to the location
      * @return false If the player can not move to the location
      */
    public boolean canMoveHere(int x, int y) {
        if (maze[y][x].getInfo().equals("X")) return false;
          
        return true;
    }
    
    /**
      * Output the current data of the map
      * 
      * @return String A whole string that contains the whole maze data
      */
    public String print() {
        StringBuilder mazeBuilder= new StringBuilder();
        
        appendMazeHeader(mazeBuilder);
        appendMazeBody(mazeBuilder);
        
        return mazeBuilder.toString();
    }
}
