import java.util.*;

/**
  * A class that stores data that is pertinent to the game flow. This class keeps track of the player's location, their path,
  * and their name. This class also controls the win condition and whether it was properly met
  * 
  * @author Bryan Nosar
  * @version 1.0 September 27, 2015
  */
class GameState {

    private Location playerLocation;
    private ArrayList<Location> playerPath;
    private Maze maze;
    private int mazeSize;
    
     /**
      *Constructor
      * 
      * @param maze A maze to be used to reference data stored from reading in a file
      * @param name A player's name
      */
    public GameState(Maze maze, String name){
        this.maze = maze;
        this.name = name;
        mazeSize = maze.getSize();
        
        playerPath = new ArrayList<Location>();
        playerPath.add(maze.getLocation(0,0));
        
        // initialize player location
        playerLocation = maze.getLocation(0,0);
        playerLocation.setInfo("P");
        playerLocation.setVisited(true);
    }

    private String name;
    
    /**
      * Get the Player's Name
      * 
      * @return name The player's name
      */
    public String getName(){
        return name;
    }
    
    /**
      * Set the Player's name
      * 
      * @param name The player's name
      */
    public void setName(String name){
        this.name = name;
    }
    
    /**
      * Get the Player's current Location
      * 
      * @return playerLocation The player's current Location in the maze
      */
    public Location getPlayerLocation(){
        return playerLocation;
    }

    /**
      * Move the player
      * 
      * @param dir The direction the player wants to move
      * @return "ongoing" The maze game is ongoing
      * @return "win" The player beats the maze
      */
    public String movePlayer(String dir) {
        int playerX = playerLocation.getX();
        int playerY = playerLocation.getY();
        
        boolean wall = false;
        boolean boundsError = false;
        boolean startOver = false;
        String errorString = "";
        
        switch(dir){
            case "U":
            case "u":
                if (playerY == 0){    
                   boundsError = true;
                   errorString = "Cannot move up! (Out of Bounds)";
                }
                else if (!maze.canMoveHere(playerX, playerY-1)) {
                   wall = true;
                   maze.getLocation(playerX,playerY-1).setVisited(true);
                   errorString = "Cannot move up! (Wall)";
                }
        
                playerY = playerY-1;
                break;
            case "D":
            case "d":
                if(playerY == mazeSize - 1){
                   boundsError = true;
                   errorString = "Cannot move down! (Out of Bounds)";
                }
                else if (!maze.canMoveHere(playerX, playerY+1)) {
                    wall = true;
                    maze.getLocation(playerX,playerY+1).setVisited(true);
                    errorString = "Cannot move down! (Wall)";
                }
 
                playerY = playerY+1;
                break;
            case "L":
            case "l":
                if(playerX == 0) {
                    boundsError = true;
                    errorString = "Cannot move left!  (Out of Bounds)";
                }
                else if ( !maze.canMoveHere(playerX-1, playerY)) {
                    wall = true;
                    maze.getLocation(playerX-1,playerY).setVisited(true);
                    errorString = "Cannot move left! (Wall)";
                }
                
                playerX = playerX-1;
                break;
            case "R":
            case "r":
                if(playerX == mazeSize - 1) {
                    boundsError = true;
                    errorString = "Cannot move right!  (Out of Bounds)";
                }
                else if (!maze.canMoveHere(playerX+1, playerY)) {
                    wall = true;
                    maze.getLocation(playerX+1,playerY).setVisited(true);
                    errorString = "Cannot move right!  (Wall)";
                }
                
                playerX = playerX+1;
                break;
            case "S":
            case "s":
            //start over
                startOver = true;
                break;
            case "G":
            case "g":
            //give up
                System.out.println("You gave up! :( \nTry again next time!");
                System.exit(0);
                break;
            default:
            //invalid input
                System.out.println("Error: Invalid Input provided!");
                break;
        }

        
        if (wall || boundsError) {
            
            System.out.println(errorString);
            return "ongoing";
        }
        
        if(startOver){
            System.out.println("\nStarting over maze!");
            playerLocation = maze.getLocation(0,0);
            adjustPath();
            return "ongoing";
        }
        
        playerLocation.setInfo("T");
        playerLocation = maze.getLocation(playerX, playerY);
        
        if (isWinCondition()) {
            adjustPath();
            return "win";
        }
        adjustPath();
        
        return "ongoing";
    }
    
    /**
      * Check win condition
      * 
      * @return true Player is at the exit
      * @return false Player is not at the exit
      */
    private boolean isWinCondition() {
        return playerLocation.getInfo().equals("E");
    }
    
    /**
      * Append or remove Locations that the Player moves to
      */
    private void adjustPath() {
    
        int removeIndex = -1; // assume we won't remove anything
        for (int i = 0; i != playerPath.size(); ++i ) {
           if (playerPath.get(i) == playerLocation) {
              removeIndex = i;
              break;
           }
        }
        
        // add to path if a new location
        if (removeIndex == -1){
            playerLocation.setInfo("P");
            playerLocation.setVisited(true);
            playerPath.add(playerLocation);
            return;
        }
        
        // otherwise remove all locations after the previously visited path
        for (int i = playerPath.size()-1; i != removeIndex; --i) {
            playerPath.get(i).setInfo(" ");
            playerPath.get(i).setVisited(false);
            playerPath.remove(i);
        }
        playerLocation.setInfo("P");
    }
    
    /**
      * Print the player's path
      * 
      * @return String The player's path they took to the exit
      */
    public String printPath(){
        StringBuilder builder = new StringBuilder();
        builder.append("Your path was:");
        for (Location location : playerPath) {
            builder.append(" ");
            builder.append("(");
            builder.append(location.getX());
            builder.append(",");
            builder.append(location.getY());
            builder.append(")");
            builder.append(",");
        }
        builder.deleteCharAt(builder.length() - 1);
        
        return builder.toString();   
         
    }
}
